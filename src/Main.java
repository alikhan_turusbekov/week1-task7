import java.util.Scanner;

public class Main {

    public static String reverseOrder(int size, int a[]) {
        if (size==1) return a[size-1]+" ";
        return a[size-1]+" "+reverseOrder(size-1, a);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int size = s.nextInt();

        int a[]= new int[size];
        for(int i=0; i<size; i++) {
            a[i] = s.nextInt();
        }

        System.out.println(reverseOrder(size, a));
    }
}
